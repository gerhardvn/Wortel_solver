# --------------------------------------------------------------------------
# remove words that do not contain input
def must_have(woorde, input) 
  woorde.select{|woord| woord.include?(input)}
end

# --------------------------------------------------------------------------
# remove words that do contain input
def must_not_have(woorde, input) 
  if !is_number?(input[0])  
  woorde.select{|woord| !woord.include?(input)} 
  elsif is_number?(input[0]) and is_lowercase_letter?(input[1])
    woorde.select{|woord| !woord[input[0].to_i].include?(input[1])} 
  end
end

# --------------------------------------------------------------------------
# check if argument is a number
def is_number? (x)
  code = x.ord
  48 <= code && code <= 57
end

# --------------------------------------------------------------------------
def is_lowercase_letter? (x)
  code = x.ord
  97 <= code && code <= 122
end

# --------------------------------------------------------------------------
# Remove words that do not contain character at position x
def numeric_position(woorde, number, character)
  woorde.select do |woord|
    woord[number-1].include?(character)
  end
end

# --------------------------------------------------------------------------
def help_text()
  # Help
  puts "p Prints word list"
  puts "+[char] Word must have at least one [char] > +z"
  puts "-[char] Word must not have any [char] >-a" 
  puts "-[digit][char] Word must not have [char] at position [digit] > -1d" 
  puts "[digit][char] Word must have [char] at position [digit] > 1z" 
  puts "? Print this message" 
  puts "q Exits" 
end

# ==========================================================================================

# read file and split words into an array
woorde = File.open('wortel_woorde.txt', &:readline).split("\"\,")
woorde.map! { |woord| woord.tr('\"','').strip() }

help_text()

input = ""
while !input.include?("q")
  print '> '
  input = gets.strip().downcase()
  
  if input.length > 0 then
    if (input[0] == '+') and is_lowercase_letter?(input[1])
    woorde = must_have(woorde, input.tr('+', ''))
    elsif (input[0] == '-') and is_lowercase_letter?(input[1])
    woorde = must_not_have(woorde, input.tr('-', ''))
    elsif (is_number?(input[0])) and is_lowercase_letter?(input[1])
    woorde = numeric_position(woorde, input[0].to_i, input[1])
  elsif (input[0] == 'p')
    puts woorde  
    elsif (input[0] == '?')
      help_text()  
  end
  
  if woorde # not filtered to null 
    puts woorde.count
  else # there is no words left
    puts "List is empty"
    input = 'q'
  end    
  end
end
