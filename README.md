# Wortel Solver

'n [Ruby](https://www.ruby-lang.org/en/) toepassing om die raaisel op [Wortel](https://wortel.wrintiewaar.co.za/) op te los.

# Woorde lys
Die woorde lys is uit [wortel](https://wortel.wrintiewaar.co.za/) se bron kode gesteel.

# Gebruik
* Minus en n letter. -d : Verwyder woorde wat `n d bevat. 
* Plus en n letter. +d : Verwyder woorde wat nie 'n D bevat.
* Nommer en n letter. 1d : Verwyder woorde wat nie 'n D in die eerste posisie bevat nie.
* p: Wys die huidig woorde lys.
* q: quit die toepassing.

Na elke stap wys die toepassing die getal woorde in die lys.

